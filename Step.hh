
// namespace nuclio\plugin\workflow
// {
// 	use nuclio\Nuclio;
// 	use nuclio\core\plugin\Plugin;
// 	use nuclio\core\ClassManager;

// 	use nuclio\plugin\session\WorkflowException;

// 	use nuclio\plugin\workflow\model\Step as StepModel;

// 	<<singleton>>
// 	class Step extends Plugin
// 	{
// 		public static function getInstance(/* HH_FIXME[4033] */...$args):Workflow
// 		{
// 			$instance=ClassManager::getClassInstance(self::class,...$args);
// 			return ($instance instanceof self)?$instance:new self(...$args);
// 		}

// 		public function create(Map<string,mixed> $record):void
// 		{
// 			$name = $record->get('name');
// 			$description = $record->get('description');
// 			$workflowId = $record->get('workflowId');

// 			if(!$name || $name=='')
// 			{
// 				throw new WorkFlowException("Step name cannot be empty");
// 			}

// 			$step = StepModel::create();
// 			$step->setName($name);
// 			$step->setDescription($description);
// 			$step->setWorkflowId($workflowId);
// 			$step->save();
// 		}

// 		public function update(Map<string,mixed> $record):void
// 		{
// 			$id = $record->get('id');
// 			$name = $record->get('name');
// 			$description = $record->get('description');

// 			if(!$name || $name=='')
// 			{
// 				throw new WorkFlowException("Step name cannot be empty");
// 			}

// 			$step = StepModel::findById($id);
// 			if(!$step)
// 			{
// 				throw new WorkflowException("Step does not exist");
// 			}
// 			$step->setName($name);
// 			$step->setDescription($description);
// 			$step->save();	
// 		}

// 		public function remove($id)
// 		{
// 			$step = StepModel::findById($id);
// 			if(!$step)
// 			{
// 				throw new WorkflowException("Step does not exist");
// 			}
// 			$set->delete();
// 		}
// 	}
// }
