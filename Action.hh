<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\workflow
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\workflow\WorkflowException;
	use nuclio\plugin\workflow\model\Action as ActionModel;

	<<singleton>>
	class Action extends Plugin
	{
		public static function getInstance(/* HH_FIXME[4033] */...$args):Action
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self();
		}

		public function create(Map<string,mixed> $record):void
		{
			$path = $record->get('path');
			$parameters = $record->get('parameters');

			$action = ActionModel::create();
			$action->setPath($path);
			$action->setParameters($parameters);
			$action->save();
		}

		public function update(Map<string,mixed> $record):void
		{
			$id = $record->get('id');
			$path = $record->get('path');
			$parameters = $record->get('parameters');

			$action = ActionModel::findById($id);
			$action->setPath($path);
			$action->setParameters($parameters);
			$action->save();
		}

		public function remove(string $id):bool
		{
			$action = ActionModel::findById($id);
			if(!$action)
			{
				throw new WorkflowException("Transition does not exist");	
			}
			$action->delete();	
			return true;
		}
	}
}
