<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\workflow
{
	use nuclio\Nuclio;
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;

	use nuclio\plugin\workflow\WorkflowException;

	use nuclio\plugin\workflow\model\Workflow as WorkflowModel;
	use nuclio\plugin\workflow\model\Step 	as StepModel;

	<<singleton>>
	class Workflow extends Plugin
	{
		public static function getInstance(/* HH_FIXME[4033] */...$args):Workflow
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self();
		}

		public function create(Map<string,mixed> $record):void
		{
			$name		=$record->get('name');
			$description=$record->get('description');
			$steps		=$record->get('steps');
			
			if ($name=='')
			{
				throw new WorkflowException("Name cannot be empty");
			}
			$workflow = WorkflowModel::create();
			if (!$steps instanceof Set)
			{
				$steps=Set{$steps};
			}
			
			$workflow->setSteps($steps);
			$workflow->setName($name);
			$workflow->setDescription($description);
			$workflow->save();
			foreach ($steps as $stepId) 
			{
				$step = StepModel::findById($stepId);
				$workflows = $step->getWorkflows();
				if($workflows instanceof Set)
				{
					$workflows->add($workflow->getId());
					$step->setWorkflows($workflows);
					$step->save();	
				}
			}
		}

		public function update(Map<string, mixed> $record):void
		{
			$id			=$record->get('id');
			$steps		=$record->get('steps');
			$name		=$record->get('name');
			$description=$record->get('description');

			if ($name=='')
			{
				throw new WorkflowException("Name cannot be empty");
			}
			$workflow=WorkflowModel::findById($id);
			if (!$workflow)
			{
				throw new WorkflowException("Workflow does not exist");
			}
			if (!$steps instanceof Set)
			{
				$steps = Set{$steps};	
			}
			$workflow->setName($name);
			$workflow->setDescription($description);
			$workflow->setSteps($steps);
			$workflow->save();
			foreach ($steps as $stepId) 
			{
				$step = StepModel::findById($stepId);
				$workflows = $step->getWorkflows();
				if($workflows instanceof Set)
				{
					$workflows->contains($id)?null:$workflows->add($id);
					$step->setWorkflows($workflows);
					$step->save();
				}
			}
		}

		public function delete(string $id):void
		{
			$workflow = WorkflowModel::findById($id);
			if(!$workflow)
			{
				throw new WorkflowException("Workflow does not exist");
			}
			$workflow->delete();
		}

		public function addStep(string $id, string $stepId):void
		{
			$workflow	=WorkflowModel::findById($id);
			$step		=StepModel::findById($stepId);
			if (!$workflow)
			{
				throw new WorkflowException("Workflow does not exist"); 
			}
			if (!$step)
			{
				//create it?
				throw new WorkflowException("Step does not exist");
			}
			$steps = $workflow->getSteps();
			if ($steps instanceof Set)
			{
				$steps->add($stepId);
				$workflow->setSteps($steps);
				$workflow->save();
				$workflows = $step->getWorkflows();
				if($workflows instanceof Set)
				{
					$workflows->add($id);
					$step->setWorkflows($workflows);
					$step->save();
				}
			}
		}

		public function removeStep(string $id, string $stepId):void
		{
			$workflow = WorkflowModel::findById($id);
			if (!$workflow)
			{
				throw new WorkflowException("Workflow does not exist");
			}
			$steps = $workflow->getSteps();
			if ($steps instanceof Set)
			{
				$steps->remove($stepId);
				$workflow->setSteps($steps);
				$workflow->save();
				$step = StepModel::findById($stepId);
				$workflows = $step->getWorkflows();
				if($workflows instanceof Set)
				{
					$workflows->remove($id);
					$step->setWorkflows($workflows);
					$step->save();
				}
			}
		}

		// public function addTransition(string $id, string $transitionId):void
		// {
		// 	$workflow = WorkflowModel::findById($id);
		// 	if(!$workflow)
		// 	{
		// 		throw new WorkflowException("Workflow does not exist");
		// 	}
		// 	$transitions = $workflow->get('transitions');
		// 	$transitions->add($transitionId);
		// 	$workflow->setTransitions($transitions);
		// 	$workflow->save();
		// }

		// public function removeTransition(string $id, string $transitionId):void
		// {
		// 	$workflow = WorkflowModel::findById($id);
		// 	if(!$workflow)
		// 	{
		// 		throw new WorkflowException("Workflow does not exist");
		// 	}
		// 	$transitions = $workflow->get('transitions');
		// 	$transitions->remove($transitionId);
		// 	$workflow->setTransitions($transitions);
		// 	$workflow->save();
		// }

		// public function doTransition(){}

		// public function getActionsForTransition(){}
	}
}
