
// namespace nuclio\plugin\workflow
// {
// 	use nuclio\Nuclio;
// 	use nuclio\core\plugin\Plugin;
// 	use nuclio\core\ClassManager;

// 	use nuclio\plugin\session\WorkflowException;

// 	use nuclio\plugin\workflow\model\Transition as TransitionModel;

// 	<<singleton>>
// 	class Transition extends Plugin
// 	{
// 		public static function getInstance(/* HH_FIXME[4033] */...$args):Workflow
// 		{
// 			$instance=ClassManager::getClassInstance(self::class,...$args);
// 			return ($instance instanceof self)?$instance:new self(...$args);
// 		}

// 		public function create(Map<string,mixed> $record):void
// 		{
// 			$from = $record->get('from');
// 			$to = $record->get('to');
// 			$actions = $record->get('actions');

// 			if(!$from || !$to)
// 			{
// 				throw new WorkFlowException("from and to cannot be empty");
// 			}

// 			$transition = TransitionModel::create();
// 			$transition->setFrom($from);
// 			$transition->setTo($to);
// 			$transition->setActions($actions);
// 			$transition->save();
// 		}

// 		public function update(Map<string,mixed> $record):void
// 		{
// 			$id = $record->get('id');
// 			$from = $record->get('from');
// 			$to = $record->get('to');
// 			$actions = $record->get('actions');

// 			if(!$from || !$to)
// 			{
// 				throw new WorkFlowException("from and to cannot be empty");
// 			}

// 			$transition = TransitionModel::findById($id);
// 			$transition->setFrom($from);
// 			$transition->setTo($to);
// 			$transition->setActions($actions);
// 			$transition->save();
// 		}

// 		public function remove($id)
// 		{
// 			$transition = TransitionModel::findById($id);
// 			if(!$transition)
// 			{
// 				throw new WorkflowException("Transition does not exist");
// 			}
// 			$transition->delete();	
// 		}

// 		// public function addAction($id, $actionId)
// 		// {
// 		// 	$transition = TransitionModel::findById($id);
// 		// 	if(!$transition)
// 		// 	{
// 		// 		throw new WorkflowException("Transition does not exist");
				
// 		// 	}
// 		// 	$actions = $transition->get('actions');
// 		// 	$actions->add($actionId);
// 		// 	$transition->setActions($actions);
// 		// 	$transition->save();	
// 		// }

// 		// public function removeAction()
// 		// {
// 		// 	$transition = TransitionModel::findById($id);
// 		// 	if(!$transition)
// 		// 	{
// 		// 		throw new WorkflowException("Transition does not exist");
// 		// 	}
// 		// 	$actions = $transition->get('actions');
// 		// 	$actions->remove($actionId);
// 		// 	$transition->setActions($actions);
// 		// 	$transition->save();
// 		// }
// 	}
// }
