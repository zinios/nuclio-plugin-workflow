<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\workflow\model
{
	use nuclio\plugin\database\orm\Model;
	
	/**
	 * @Collection stepTransitions
	 */
	class StepTransition extends Model
	{
		/*
		 * @Id(strategy="AUTO")
		 */ 
		public ?string $id=null;

		/*
		 *@Relate nuclio\plugin\workflow\model\Transition
		 */
		public ?string $transitionId=null;
		
		/**
		 * @Relate nuclio\plugin\workflow\model\Step
		 */
		public ?string $from=null;

		/**
		 * @Relate nuclio\plugin\workflow\model\Step
		 */
		public ?string $to=null;
	}
}
