<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\workflow\model
{
	use nuclio\plugin\database\orm\Model;
	
	/**
	 * @Collection stepTransitionActions
	 */
	class StepTransitionAction extends Model
	{
		/*
		 * @Id(strategy="AUTO")
		 */ 
		public ?string $id=null;
		
		/**
		 * @Relate nuclio\plugin\workflow\model\StepTransition
		 */
		public ?string $stepTransition=null;

		/**
		 * @Relate nuclio\plugin\workflow\model\Action
		 */
		public ?string $action=null;

		/**
		 * @String
		 */
		public ?string $params=null;

		/**
		 * @String
		 */
		public ?string $order=null;
	}
}
