<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\workflow\model
{
	use nuclio\plugin\database\orm\Model;
	
	/**
	 * @Collection transition
	 */
	class Transition extends Model
	{
		/*
		 * @Id
		 */ 
		public ?string $id=null;
		
		/**
		 * @String
		 */
		public ?string $name=null;

		/**
		 * @String
		 */
		public ?string $description=null;

		/**
		 * @String`
		 */
		public ?string $status=null;
	}
}
