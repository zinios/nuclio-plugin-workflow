<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\workflow\model
{
	use nuclio\plugin\database\orm\Model;
	
	/**
	 * @Collection steps
	 */
	class Step extends Model
	{
		/*
		 * @Id(strategy="AUTO")
		 */
		public ?string $id=null;
		
		/**
		 * @String
		 */
		public ?string $name=null;

		/**
		 * @String
		 */
		public ?string $description=null;

		/*
		 * @Relate nuclio\plugin\workflow\model\Workflow
		 */
		public ?string $workflow=null;
	}
}
